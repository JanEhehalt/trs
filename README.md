# The Restless Simulations

This LibGDX-Project is a Framework for programming different simulations and collecting them inside one project.

#### Project Status

This project is currently not being actively developed and is in a stable state. It is considered complete for its current purpose.

#### Compiling
- Desktop
    running the gradlew executable like this:
        ```./gradlew desktop:dist```
    A executable file will be created at desktop/build/libs
- Android
    using Android Studio -> apk

#### Simulations

Currently there are only two Simulations

##### Gravity

A simulation of planetary gravity using real physical formulas for calculation. The Planets start at random positions and then calculations are run. If planets are close enough together they merge together into a bigger planet with bigger mass and stronger gravitational field.

##### Virus

This simulations the spreading of a disease using a highly simplified logic. The simulations starts with some random people who are infected. If they are close enough to uninfected people there is a chance that they will also be infected and spread the disease further. After some time a infected person will gain immunity or die, which the simulation does not differentiate. Infected are displayed red, immune/dead gray.

package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.buttons.Button;
import com.mygdx.game.simulations.Gravity;
import com.mygdx.game.simulations.Virus;

public class controller extends ApplicationAdapter implements InputProcessor {

	final float GAME_WORLD_WIDTH = 1600;
	final float GAME_WORLD_HEIGHT = 900;


	SpriteBatch batch;
	ShapeRenderer renderer;
	BitmapFont font;
	Simulation sim;
	Viewport viewport;
	OrthographicCamera camera;

	int previousX;

	@Override
	public void create () {
		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		renderer.setColor(Color.BLACK);
		font = generateBitmapFont();
		font.setColor(Color.BLACK);
		sim = new MainMenu();
		camera = new OrthographicCamera();
		viewport = new StretchViewport(GAME_WORLD_WIDTH, GAME_WORLD_HEIGHT, camera);
		viewport.apply();
		camera.position.set(GAME_WORLD_WIDTH/2, GAME_WORLD_HEIGHT/2, 0);
		camera.update();
/*
		Matrix4 mat = camera.combined.cpy();
		mat.setToOrtho2D(0, 0, GAME_WORLD_WIDTH, GAME_WORLD_HEIGHT);
		renderer.setProjectionMatrix(mat);
		batch.setProjectionMatrix(mat);
*/


		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
		renderer.setProjectionMatrix(camera.combined);
		camera.update();
		if(sim.getSimulationToStart() != -1){
			switch(sim.getSimulationToStart()){
				case 0:
					camera.position.set(GAME_WORLD_WIDTH/2, GAME_WORLD_HEIGHT/2, 0);
					camera.update();
					sim.stop();
					sim = null;
					sim = new MainMenu();
					break;
				case 1:
					camera.position.set(GAME_WORLD_WIDTH/2, GAME_WORLD_HEIGHT/2, 0);
					camera.update();
					sim.stop();
					sim = null;
					sim = new Gravity();
					break;
				case 2:
					camera.position.set(GAME_WORLD_WIDTH/2, GAME_WORLD_HEIGHT/2, 0);
					camera.update();
					sim.stop();
					sim = null;
					sim = new Virus(8000,100,5,0.5,0.0037);
					break;
			}
		}
		if(camera.position.x < GAME_WORLD_WIDTH / 2 && sim.getCameraMovement() < 0){

		}
		else if(camera.position.x > sim.getScreenWidth() - GAME_WORLD_WIDTH/2 && sim.getCameraMovement() > 0){

		}
		else{
			camera.translate(sim.getCameraMovement(),0,0);
		}
		sim.render(batch, renderer, font, new Vector2(camera.position.x, camera.position.y));
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}

	public BitmapFont generateBitmapFont(){
		BitmapFont font;
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 21;
		font = generator.generateFont(parameter);
		generator.dispose();
		return font;
	}

	@Override
	public boolean keyDown(int keycode) {
		if(sim != null){
			sim.keyDown(keycode);
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(sim != null){
			sim.keyUp(keycode);
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		if(sim != null){
			sim.keyTyped(character);
		}
		return true;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(sim != null){
			sim.touchDown(screenX, screenY, pointer, button);
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(sim != null){
			sim.touchUp(screenX, screenY, pointer, button);
		}
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if(sim != null){
			sim.touchDragged(screenX, screenY, pointer);
		}
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		if(sim != null){
			sim.mouseMoved(screenX, screenY);
		}
		return true;
	}

	@Override
	public boolean scrolled(int amount) {
		if(sim != null){
			sim.scrolled(amount);
		}
		return true;
	}
}

package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public interface Simulation {

    public void start();

    public void stop();

    public void reset();

    public void dispose();

    public void render(SpriteBatch batch, ShapeRenderer renderer, BitmapFont font, Vector2 cameraPosition);

    public boolean keyDown(int keycode);

    public boolean keyUp(int keycode);

    public boolean keyTyped(char character);

    public boolean touchDown(int screenX, int screenY, int pointer, int button);

    public boolean touchUp(int screenX, int screenY, int pointer, int button);

    public boolean touchDragged(int screenX, int screenY, int pointer);

    public boolean mouseMoved(int screenX, int screenY);

    public boolean scrolled(int amount);

    public int getCameraMovement();

    public int getScreenWidth();

    public int getSimulationToStart();

}

package com.mygdx.game.simulations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.Simulation;
import com.mygdx.game.buttons.Button;

import java.util.ArrayList;

public class Virus implements Simulation {

    private final int GAME_WORLD_WIDTH = 1600;
    private final int GAME_WORLD_HEIGHT = 900;

    private int entityAmount;
    private int startInfections;
    private int size;
    private double infectionRisk;
    private double deathRate;
    private boolean drawHealed;
    private Entity[] field;
    private ArrayList<Button> buttons;
    private int simulationToStart;
    private Graph graph;



    Timer timer;


    public Virus(int entityAmount, int startInfections, int size, double infectionRisk, double deathRate){
        this.entityAmount = entityAmount;
        this.startInfections = startInfections;
        this.size = size;
        this.infectionRisk = infectionRisk;
        this.deathRate = deathRate;
        drawHealed = true;
        field = new Entity[entityAmount];
        simulationToStart = -1;

        graph = new Graph(100,20, 1400,100);

        buttons = new ArrayList<>();
        buttons.add(new Button(50,800,50,50,"<",0, 2));

        for(int i = 0; i < field.length; i++){
            if(i < startInfections){
                field[i] = new Entity((int) (5*Math.round(Math.random() * (GAME_WORLD_WIDTH/5))),(int) (5* Math.round(Math.random() * (GAME_WORLD_HEIGHT*0.4))), 1);
            }
            else{
                field[i] = new Entity((int) (5*Math.round(Math.random() * (GAME_WORLD_WIDTH/5))),(int) (5* Math.round(Math.random() * (GAME_WORLD_HEIGHT/5))), 0);
            }
        }

        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                simulateDay();
            }
        }, 0, 0.15f);
    }

    @Override
    public void start() {
        timer.start();
    }

    @Override
    public void stop() {
        timer.clear();
    }

    @Override
    public void reset() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void render(SpriteBatch batch, ShapeRenderer renderer, BitmapFont font, Vector2 cameraPosition) {
        draw(renderer);
        for(Button button : buttons){
            button.draw(batch, renderer, font);
        }
        graph.drawGraph(renderer);
        renderer.end();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int but) {
        screenX = (int)(1600f * (float)screenX / (float) Gdx.graphics.getWidth());
        screenY = Gdx.graphics.getHeight() - screenY;
        screenY = (int)(900f * (float)screenY / (float)Gdx.graphics.getHeight());
        System.out.println(screenX + "   " + screenY);
        for(Button button : buttons){
            if(button.overlaps(screenX,screenY)){
                simulationToStart = button.getId();
            }
        }
        System.out.println(simulationToStart);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public int getCameraMovement() {
        return 0;
    }

    @Override
    public int getScreenWidth() {
        return 0;
    }

    @Override
    public int getSimulationToStart() {
        return simulationToStart;
    }
    public void draw(ShapeRenderer renderer){
        if(!renderer.isDrawing()){
            renderer.begin(ShapeRenderer.ShapeType.Filled);
        }
        for(int i = 0; i < field.length; i++){
            if(field[i].getStatus() == 0){
                renderer.setColor(Color.BLACK);
                renderer.rect(field[i].getX(), field[i].getY(), size, size);
            }
            if(field[i].getStatus() == 1){
                renderer.setColor(Color.RED);
                renderer.rect(field[i].getX(), field[i].getY(), size, size);
            }
            if(field[i].getStatus() == 2 && drawHealed){
                renderer.setColor(Color.GRAY);
                renderer.rect(field[i].getX(), field[i].getY(), size, size);
            }
        }
        renderer.setColor(Color.BLACK);

    }
    public void drawGraph(ShapeRenderer renderer){

    }
    public void simulateDay(){
        for(int i = 0; i < field.length; i++){
            double dead = Math.random();
            if(dead <= field[i].getDeathChance()){
                field[i].heal();
                field[i].increaseInfectionTime();

            }
            else{
                field[i].move();
                if(field[i].getStatus() == 1){
                    field[i].increaseInfectionTime();
                }
            }
        }
        for(int i = 0; i < field.length; i++){
            for(int n = 0; n < field.length; n++){
                if(i != n && field[i].getX() == field[n].getX() && field[i].getY() == field[n].getY()){
                    if(field[i].getStatus() == 2 || field[n].getStatus() == 2){

                    }
                    else if(field[i].getStatus() == 1 || field[n].getStatus() == 1){
                        double infect = Math.random();
                        if(infect <= infectionRisk){
                            field[i].infect();
                            field[n].infect();
                        }
                    }
                }
            }
        }
        graph.calc();
    }

    private class Entity{
        int x;
        int y;
        int status;
        int infectionTime;
        double deathChance;
        public Entity(int x, int y, int status){
            this.x = x;
            this.y = y;
            this.status = status;
            infectionTime = 0;
            deathChance = 0;
        }
        public int getX() {return x;}
        public void setX(int x) {this.x = x;}

        public int getY() {return y;}

        public void setY(int y) {this.y = y; }

        public int getStatus() { return status; }

        public void setStatus(int status) { this.status = status; }

        public int getInfectionTime() { return infectionTime; }

        public void setInfectionTime(int infectionTime) { this.infectionTime = infectionTime; }

        public double getDeathChance() {
            if(this.status == 1){
                this.deathChance = deathRate;
            }
            return deathChance;
        }

        public void setDeathChance(double deathChance) { this.deathChance = deathChance; }

        public void infect(){ this.status = 1;}
        public void heal(){this.status = 2;}
        public void increaseInfectionTime(){ this.infectionTime = this.infectionTime+1;}
        public void move(){
            double lolol = Math.random();
            if(this.status != 2){
                    if(this.status == 2 && !drawHealed){
                    }
                    else{
                        if(lolol <= 0.25){
                            if(this.y > 0){
                                this.y = this.y - 5;
                            }
                        }
                        else if(lolol > 0.25 && lolol <= 0.5){
                            if(this.x < GAME_WORLD_WIDTH){
                                this.x = this.x + 5;
                            }
                        }
                        else if(lolol > 0.5 && lolol <= 0.75){
                            if(this.y < GAME_WORLD_HEIGHT){
                                this.y = this.y + 5;
                            }
                        }
                        else if(lolol <= 1){
                            if(this.x > 0){
                                this.x = this.x - 5;
                            }
                        }
                    }
            }
        }
    }

    private class Graph{
        int graphPoint;
        int width;
        int height;
        int xPos;
        int yPos;
        private float[] infectedPerc;
        private float[] cleanPerc;
        private float[] healthyPerc;
        public Graph(int x, int y, int width, int height){
            this.xPos = x;
            this.yPos = y;
            this.width = width;
            this.height = height;
            infectedPerc = new float[width];
            cleanPerc = new float[width];
            healthyPerc = new float[width];
            for(int i = 0; i < width; i++){
                infectedPerc[i] = 0;
                cleanPerc[i] = 0;
                healthyPerc[i] = 0;
            }
        }
        public void drawGraph(ShapeRenderer renderer){
            if(!renderer.isDrawing()){
                renderer.begin(ShapeRenderer.ShapeType.Filled);
            }
            renderer.setColor(Color.WHITE);
            renderer.rect(xPos,yPos,width,height);
            renderer.setColor(Color.BLACK);
            for(int i = 0; i < width; i++){
                    renderer.rect(xPos+i, yPos + infectedPerc[i]*height + healthyPerc[i]*height, 1, cleanPerc[i]*height);
            }
            renderer.setColor(Color.RED);
            for(int i = 0; i < width; i++){
                    renderer.rect(xPos+i, yPos + healthyPerc[i]*height, 1, infectedPerc[i]*height);
            }
            renderer.setColor(Color.GRAY);
            for(int i = 0; i < width; i++){
                    renderer.rect(xPos+i, yPos, 1, healthyPerc[i]*height);
            }
            renderer.setColor(Color.DARK_GRAY);
            renderer.rectLine(xPos, yPos, xPos+width, yPos, 2);                               // BOTTOM
            renderer.rectLine(xPos, yPos, xPos, yPos + height, 2);                            // LEFT
            renderer.rectLine(xPos + width, yPos, xPos + width, yPos + height, 2);     // RIGHT
            renderer.rectLine(xPos, yPos + height, xPos + width, yPos + height, 2);   // TOP
        }
        public void calc(){
            int clean = 0;
            int infected = 0;
            int healthy = 0;

            for(int i = 0; i< field.length; i++){
                if(field[i].getStatus() == 0){
                    clean++;
                }
                if(field[i].getStatus() == 1){
                    infected++;
                }
                if(field[i].getStatus() == 2){
                    healthy++;
                }
            }
            cleanPerc[graphPoint] = (float)clean / (float)field.length;
            infectedPerc[graphPoint] = (float)infected / (float)field.length;
            healthyPerc[graphPoint] = (float)healthy / (float)field.length;
            graphPoint++;
        }
    }



}

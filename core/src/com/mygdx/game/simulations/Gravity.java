package com.mygdx.game.simulations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.Simulation;
import com.mygdx.game.buttons.Button;

import java.util.ArrayList;
import java.util.LinkedList;

public class Gravity implements Simulation {

    private Space space;
    private final double G = 5;
    private LinkedList<Vector2> traces;
    private final int WIDTH = 1600;
    private final int HEIGHT = 900;
    private Timer timer;
    private int simulationToStart;
    private ArrayList<Button> buttons;

    public Gravity(){
        simulationToStart = -1;
        buttons = new ArrayList<>();
        buttons.add(new Button(50,800,50,50,"<",0, 2));

        space = new Space();
        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                for(int i = space.planets.size() - 1; i >= 0; i--){
                    space.calculateNewPos(i);
                }

                for(int i = space.planets.size() - 1; i >= 0; i--){
                    space.planets.get(i).updatePos();
                }
            }
        }, 0, 0.02f);

        for(int i = 1; i < 20; i++){
            space.planets.add(new Planet(8, (Math.random()*WIDTH), (Math.random()*HEIGHT), (Math.random()*2)-1, (Math.random()*2)-1));
        }
    }

    private class Planet{
        double mass;
        double radius;
        double xPos;
        double yPos;
        double vX;
        double vY;

        public Planet(double m, double xPos, double yPos, double vX, double vY){
            this.mass = m;
            this.radius = (Math.sqrt(this.mass / Math.PI)) * 3;
            this.xPos = xPos;
            this.yPos = yPos;
            this.vX = vX;
            this.vY = vY;
        }

        public void updateMovement(double changeX, double changeY){
            this.vX += changeX;
            this.vY += changeY;
        }

        public void updatePos(){
            this.xPos += this.vX;
            this.yPos += vY;
        }
    }

    private class Space{
        ArrayList<Planet> planets;

        public Space(){
            planets = new ArrayList<>();
            traces = new LinkedList<>();
        }

        public void calculateNewPos(int i){
            Planet point1 = this.planets.get(i);

            Vector2 temp = new Vector2();
            temp.x = (float) point1.xPos;
            temp.y = (float) (point1.yPos - point1.radius);
            traces.push(temp);

            for(int j = planets.size() - 1; j >= 0; j--){
                if(j != i){
                    Planet point2 = this.planets.get(j);

                    double deltaX = point2.xPos - point1.xPos;
                    double deltaY = point2.yPos - point1.yPos;

                    double angle;
                    if(deltaX == 0 && deltaY >= 0){
                        angle = Math.PI / 2;
                    }
                    else if (deltaX == 0 && deltaY <= 0){
                        angle = Math.PI / -2;
                    }
                    else if(deltaY == 0 && deltaX >= 0){
                        angle = 0;
                    }
                    else if(deltaY == 0 && deltaX < 0){
                        angle = Math.PI;
                    }
                    else{
                        angle = Math.abs(Math.atan(deltaY / deltaX));

                        if(deltaX < 0 && deltaY < 0){
                            angle = Math.PI + angle;
                        }
                        else if(deltaX < 0 && deltaY > 0){
                            angle = Math.PI - angle;
                        }
                        else if(deltaX > 0 && deltaY < 0){
                            angle = 2 * Math.PI - angle;
                        }
                    }

                    double r;
                    if(angle == 0 || angle == Math.PI){
                        r = deltaX;
                    }
                    else if(angle == Math.PI / 2 || angle == Math.PI / -2){
                        r = deltaY;
                    }
                    else{
                        r = deltaY / Math.sin(angle);
                    }

                    double fg = G * (point1.mass * point2.mass) / Math.pow(r, 2);
                    double changeX = Math.cos(angle) * (fg / point1.mass);
                    double changeY = Math.sin(angle) * (fg / point1.mass);

                    if(overlaps(point1, point2, r)){
                        Planet newPoint = new Planet(point1.mass + point2.mass, 0, 0, 0, 0);

                        double resX = (point1.mass * point1.vX + point2.mass * point2.vX) / (point1.mass + point2.mass);
                        double resY = (point1.mass * point1.vY + point2.mass * point2.vY) / (point1.mass + point2.mass);

                        newPoint.vX = resX;
                        newPoint.vY = resY;

                        if(point1.mass >= point2.mass){
                            newPoint.xPos = point1.xPos;
                            newPoint.yPos = point1.yPos;
                        }
                        else{
                            newPoint.xPos = point2.xPos;
                            newPoint.yPos = point2.yPos;
                        }

                        planets.set(i, newPoint);
                        planets.remove(j);

                        break;
                    }

                    point1.updateMovement(changeX, changeY);
                }
            }
        }
    }

    public boolean overlaps(Planet point1, Planet point2, double radius){
        if(Math.abs(radius) - (point1.radius + point2.radius) <= 0){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void start() {
        timer.start();
    }

    @Override
    public void stop() {
        timer.stop();
    }

    @Override
    public void reset() {
        dispose();
        space = new Space();
    }

    @Override
    public void dispose() {
        timer.clear();
    }

    @Override
    public void render(SpriteBatch batch, ShapeRenderer renderer, BitmapFont font, Vector2 cameraPosition) {
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        for(int i = 0; i < space.planets.size(); i++){
            renderer.circle((float) space.planets.get(i).xPos, (float) space.planets.get(i).yPos, (float) space.planets.get(i).radius);
        }

        for(int i = 0; i < traces.size(); i++){
            renderer.rect(traces.get(i).x, traces.get(i).y, 1,1);
        }

        for(Button button : buttons){
            button.draw(batch, renderer, font);
        }
        renderer.end();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int but) {
        screenX = (int)(1600f * (float)screenX / (float)Gdx.graphics.getWidth());
        screenY = Gdx.graphics.getHeight() - screenY;
        screenY = (int)(900f * (float)screenY / (float)Gdx.graphics.getHeight());
        for(Button button : buttons){
            if(button.overlaps(screenX,screenY)){
                simulationToStart = button.getId();
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public int getCameraMovement() {
        return 0;
    }

    @Override
    public int getScreenWidth() {
        return 1600;
    }

    @Override
    public int getSimulationToStart() {
        return simulationToStart;
    }
}

package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.buttons.Button;

import java.util.ArrayList;

public class MainMenu implements Simulation {

    final int cameraSpeed = 8;

    int width;
    ArrayList<Button> buttons;
    int space;
    int buttonWidth;
    int buttonHeight;

    float cameraX;
    float cameraY;

    int previousX;
    int previousY;

    int screenWidth;

    int cameraMovement;

    int simulationToStart;


    public MainMenu(){

        simulationToStart = -1;
        space = 50;

        buttons = new ArrayList<>();
        buttons.add(new Button(50, 300,400,300,"Gravity", 1, 2));
        buttons.add(new Button(500, 300,400,300,"Virus", 2, 2));
        buttons.add(new Button(950, 300,400,300,"- WiP -", 3, 2));
        buttons.add(new Button(1400, 300,400,300,"- WiP -", 4, 2));

    }
    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void reset() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void render(SpriteBatch batch, ShapeRenderer renderer, BitmapFont font, Vector2 cameraPosition) {
        cameraX = cameraPosition.x;
        cameraY = cameraPosition.y;
        for(Button button : buttons){
            button.draw(batch, renderer, font);
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.RIGHT){
            cameraMovement = cameraSpeed;
        }
        if(keycode == Input.Keys.LEFT){
            cameraMovement = -cameraSpeed;
        }

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        cameraMovement = 0;
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int but) {
        screenX = (int)cameraX - Gdx.graphics.getWidth()/2 + screenX;
        screenY = Gdx.graphics.getHeight() - screenY;
        screenY = (int)cameraY - Gdx.graphics.getHeight()/2 + screenY;
        //System.out.println(screenX);
        //System.out.println(screenY);
        for(Button button : buttons){
            if(button.overlaps(screenX,screenY)){
                simulationToStart = button.getId();
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        cameraMovement = 0;
        previousX = -50;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (previousX == -50){
            previousX = screenX;
        }
        cameraMovement = previousX - screenX;

        previousX = screenX;
        previousY = screenY;
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {

        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public int getCameraMovement() {
        return cameraMovement;
    }

    @Override
    public int getScreenWidth(){
        int width = 0;
        width += space;
        for(Button button : buttons){
            width += button.getWidth() + space;
        }
        if(width < 1600){
            return 1600;
        }
        else{
            return width;
        }

    }

    @Override
    public int getSimulationToStart() {
        return simulationToStart;
    }
}

package com.mygdx.game.buttons;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import org.w3c.dom.css.Rect;

public class Button {

    int xPos;
    int yPos;
    int width;
    int height;
    String text;
    int id;
    int borderThickness;

    Rectangle hitbox;

    public Button(int xPos, int yPos, int width, int height, String text, int id, int borderThickness){
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.text = text;
        this.id = id;
        this.borderThickness = borderThickness;
        hitbox = new Rectangle(xPos, yPos, width, height);
    }

    public void draw(SpriteBatch batch, ShapeRenderer renderer, BitmapFont font){
        // BORDER
        if(batch.isDrawing()){
            batch.end();
        }
        if(!renderer.isDrawing()){
            renderer.begin(ShapeRenderer.ShapeType.Filled);
        }
        renderer.setColor(Color.WHITE);
        renderer.rect(xPos, yPos, width, height);
        renderer.setColor(Color.BLACK);
        renderer.rectLine(xPos, yPos, xPos+width, yPos, borderThickness);                               // BOTTOM
        renderer.rectLine(xPos, yPos, xPos, yPos + height, borderThickness);                            // LEFT
        renderer.rectLine(xPos + width, yPos, xPos + width, yPos + height, borderThickness);     // RIGHT
        renderer.rectLine(xPos, yPos + height, xPos + width, yPos + height, borderThickness);   // TOP
        renderer.end();
        //
        // TEXT
        batch.begin();
        font.draw(batch,text, xPos + (width/2) - (getTextWidth(text, font)/2), yPos + (height/2) + getTextHeight(text, font) / 2);
        batch.end();
        //
    }

    public boolean overlaps(int x, int y){
        if(Intersector.overlaps(hitbox, new Rectangle(x,y,1,1)))
            return true;
        else
            return false;
    }

    public float getTextWidth(String text, BitmapFont font){
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font,text);
        return glyphLayout.width;
    }
    public float getTextHeight(String text, BitmapFont font){
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font,text);
        return glyphLayout.height;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBorderThickness() {
        return borderThickness;
    }

    public void setBorderThickness(int borderThickness) {
        this.borderThickness = borderThickness;
    }

    public Rectangle getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rectangle hitbox) {
        this.hitbox = hitbox;
    }
}

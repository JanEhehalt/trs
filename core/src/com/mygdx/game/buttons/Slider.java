package com.mygdx.game.buttons;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Slider {

    int xPos;
    int yPos;
    int width;
    int height;
    String text;
    float status; // 0.00 - 1
    int id;
    int borderThickness;

    public Slider(int xPos, int yPos, int width, int height, String text, int id, int borderThickness){
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.text = text;
        this.id = id;
        this.borderThickness = borderThickness;
    }


    public void draw(SpriteBatch batch, ShapeRenderer renderer, BitmapFont font){
        // BORDER
        if(batch.isDrawing()){
            batch.end();
        }
        if(!renderer.isDrawing()){
            renderer.begin(ShapeRenderer.ShapeType.Filled);
        }
        renderer.rectLine(xPos, yPos, xPos+width, yPos, borderThickness);                               // BOTTOM
        renderer.rectLine(xPos, yPos, xPos, yPos + height, borderThickness);                            // LEFT
        renderer.rectLine(xPos + width, yPos, xPos + width, yPos + height, borderThickness);     // RIGHT
        renderer.rectLine(xPos, yPos + height, xPos + width, yPos + height, borderThickness);   // TOP
        renderer.end();
        //
        // TEXT
        batch.begin();

        batch.end();
        //
    }

    public void draw(ShapeRenderer renderer){

    }

    public float getTextWidth(String text, BitmapFont font){
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font,text);
        return glyphLayout.width;
    }
    public float getTextHeight(String text, BitmapFont font){
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font,text);
        return glyphLayout.height;
    }
}
